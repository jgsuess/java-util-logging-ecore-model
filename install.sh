#!/usr/bin/env bash

export P2_REPOSITORIES=\
https://download.eclipse.org/releases/latest,\
https://download.eclipse.org/eclipse/updates/latest,\
https://download.eclipse.org/epsilon/updates,\
https://download.eclipse.org/tools/orbit/downloads/latest-R,\
https://download.eclipse.org/modeling/gmp/gmf-tooling/updates/releases/,\
https://download.eclipse.org/webtools/repository/latest/,\
https://download.eclipse.org/emfatic/update \

export INSTALLABLE_UNITS=org.eclipse.wst.ws_ui.feature.feature.group

/epsilon/eclipse \
-nosplash \
-application org.eclipse.equinox.p2.director \
-repository ${P2_REPOSITORIES} \
-installIU ${INSTALLABLE_UNITS} \
-destination /epsilon
